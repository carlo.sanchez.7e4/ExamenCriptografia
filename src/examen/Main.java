package examen;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) {
        String original = "a7ab1b4b01b34968aafd15432c3e6ed33004e57159fa04dd0f2bab267180aa44";

        String algorithm = "SHA3-256";

        // get file path from resources
        String filePath1 = ClassLoader.getSystemResource("monalisa3.jpg").getFile();
        String filePath2 = ClassLoader.getSystemResource("monalisa4.jpg").getFile();
        String filePath3 = ClassLoader.getSystemResource("monalisa5.jpg").getFile();

        String[] files = {filePath1, filePath2, filePath3};
        int counter = 3;
        for (String i : files){
            byte[] hashInBytes = checksum(i, algorithm);
            if (bytesToHex(hashInBytes).equals(original)){
                System.out.printf("La imatge monalisa%d coincideix!\n", counter);
            } else {
                System.out.printf("La imatge monalisa%d no coincideix...\n", counter);
            }
            counter++;
        }
    }

    private static byte[] checksum(String filePath, String algorithm) {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }

        try (InputStream is = new FileInputStream(filePath);
            DigestInputStream dis = new DigestInputStream(is, md)) {
            while (dis.read() != -1) ; //empty loop to clear the data
            md = dis.getMessageDigest();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return md.digest();

    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}